Windows OpenSSH installer
https://github.com/PowerShell/Win32-OpenSSH/wiki/Install-Win32-OpenSSH-Using-MSI

Setting up OpenSSH SFTP server when installed (steps I followed from ChatGTP)

### Step 1: Install OpenSSH Server on Windows
1. **Open Settings**: Go to `Settings` > `Apps` > `Optional features`.
2. **Add a Feature**: Click on `Add a feature`.
3. **Install OpenSSH Server**: Scroll down, find `OpenSSH Server`, and click `Install`.

### Step 2: Start the OpenSSH Server Service
1. **Open PowerShell as Administrator**: Right-click on the Start button and select `Windows PowerShell (Admin)`.
2. **Start the Service**: Run the following commands to start the OpenSSH server service and to configure it to start automatically on system boot:

    ```powershell
    Start-Service sshd
    Set-Service -Name sshd -StartupType 'Automatic'
    ```

3. **Open the Firewall for SSH**: Ensure that the firewall allows SSH connections:

    ```powershell
    New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22
    ```

### Step 3: Configure SSH Server
1. **Edit SSH Configuration File**: Open the `sshd_config` file to configure the SSH server.

    ```powershell
    notepad "$env:ProgramData\ssh\sshd_config"
    ```

2. **Configure SFTP Subsystem**: Ensure the following line is present and uncommented to enable the SFTP subsystem:

    ```plaintext
    Subsystem   sftp    sftp-server.exe
    ```

3. **Save and Close the File**.

### Step 4: Create Users and Configure Home Directories
1. **Create a User**: You can create a new user for SFTP access via the Windows user management tools or using PowerShell.

    ```powershell
    net user sftpuser YourPassword /add
    ```

2. **Set Home Directory Permissions**: Create a directory for the SFTP user and set the appropriate permissions.

    ```powershell
    mkdir C:\SFTP\sftpuser
    icacls C:\SFTP\sftpuser /grant sftpuser:F
    ```

3. **Configure Home Directory**: You can configure the user's home directory by adding the following to `sshd_config` (replace `sftpuser` with the actual username):

    ```plaintext
    Match User sftpuser
    ChrootDirectory C:\SFTP\sftpuser
    ForceCommand internal-sftp
    AllowTcpForwarding no
    ```

### Step 5: Restart the SSH Server
After making changes to `sshd_config`, restart the SSH server to apply the changes.

```powershell
Restart-Service sshd
```

### Step 6: Connect to the SFTP Server
Now, you can connect to the SFTP server using an SFTP client like FileZilla or via the command line.

**Using the command line:**

```sh
sftp sftpuser@hostname
```

Replace `sftpuser` with the username you created and `hostname` with the IP address or hostname of your Windows machine.

### Troubleshooting
- **Firewall**: Ensure that port 22 is open on your firewall.
- **Permissions**: Verify that the SFTP user has the necessary permissions to access their home directory.
- **Logs**: Check the SSH server logs for any errors. Logs can be found in the `Event Viewer` under `Applications and Services Logs` > `OpenSSH`.

By following these steps, you should have a functional OpenSSH SFTP server running on your Windows machine.