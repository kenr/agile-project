from main import *
import io
import unittest
import unittest.mock

#  Unit tests
class TestFunction(unittest.TestCase):
    def test_user_pass(self):
        with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            user_pass('testuser', 'testpass')
            self.assertEqual(
                mock_stdout.getvalue(),
                'Username and password set.\n'  # It's important to remember about '\n'
            )

    def test_change_path(self):
      with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
          change_path('testlocalpath', 'testuploadpath')
          self.assertEqual(
              mock_stdout.getvalue(),
              'Paths set.\n'  # It's important to remember about '\n'
          )

