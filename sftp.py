import os
import pysftp
import paramiko

# Configuration
host = "SERVER IP ADDRESS"
port = 22

cnopts = pysftp.CnOpts()
cnopts.hostkeys = None

def user_pass():
    username = input("Enter your username: ")
    password = input("Enter your password: ")
    return username, password

def connect(username, password):
    try:
        sftp = pysftp.Connection(host=host, username=username, password=password, port=port, cnopts=cnopts)
        print("Connection Successful.")
        return sftp
    except Exception as e:
        print(f"Failed to connect: {e}")
        return None

def list_all(sftp):
    print("Files and directories in the root directory:")
    for item in sftp.listdir():
        print(item)

def list_all_local():
    path = input("Enter the path to the local directory: ")
    try:
        for item in os.listdir(path):
            item_path = os.path.join(path, item)
            if os.path.isfile(item_path):
                print(f"File: {item}")
            elif os.path.isdir(item_path):
                print(f"Directory: {item}")
    except Exception as e:
        print(f"Failed to list local files and directories: {e}")

def upload_file(sftp):
    local_path = input("Enter the full path to the local file (including file name): ")
    remote_path = input("Enter the full path to the remote file (including file name): ")
    try:
        sftp.put(local_path, remote_path)
        print("File successfully uploaded to the server.")
    except Exception as e:
        print(f"Failed to upload file: {e}")

def upload_files(sftp):
    local_files = input("Enter the full paths to the local files, separated by commas: ").split(',')
    remote_directory = input("Enter the path to the remote directory: ")
    try:
        for local_file in local_files:
            local_file = local_file.strip()
            if os.path.isfile(local_file):
                remote_path = os.path.join(remote_directory, os.path.basename(local_file))
                sftp.put(local_file, remote_path)
                print(f"File {local_file} successfully uploaded to {remote_path}")
            else:
                print(f"Local file {local_file} does not exist or is not a file.")
    except Exception as e:
        print(f"Failed to upload files: {e}")

def download_file(sftp):
    remote_path = input("Enter the full path to the remote file (including file name): ")
    local_directory = input("Enter the local directory to save the file: ")
    try:
        local_path = os.path.join(local_directory, os.path.basename(remote_path))
        sftp.get(remote_path, local_path)
        print(f"File successfully downloaded from {remote_path} to {local_path}.")
    except Exception as e:
        print(f"Failed to download file: {e}")

def download_files(sftp):
    remote_files = input("Enter the full paths to the remote files, separated by commas: ").split(',')
    local_directory = input("Enter the path to the local directory: ")
    try:
        for remote_file in remote_files:
            remote_file = remote_file.strip()
            local_path = os.path.join(local_directory, os.path.basename(remote_file))
            sftp.get(remote_file, local_path)
            print(f"File {remote_file} successfully downloaded to {local_path}")
    except Exception as e:
        print(f"Failed to download files: {e}")

def delete_file(sftp):
    remote_path = input("Enter the path to the remote file to delete (including file name): ")
    try:
        sftp.remove(remote_path)
        print("File successfully deleted from the server.")
    except FileNotFoundError:
        print(f"File not found: {remote_path}")
    except PermissionError:
        print(f"Permission denied: {remote_path}")
    except Exception as e:
        print(f"Failed to delete file: {e}")

def create_directory(sftp):
    remote_path = input("Enter the path/directory name you would like to create: ")
    try:
        sftp.mkdir(remote_path)
        print("Directory successfully created on the server.")
    except Exception as e:
        print(f"Failed to create directory: {e}")

def change_permissions(username, password):
    remote_path = input("Enter the full path to the remote file (including file name): ")
    permissions = input("Enter the permissions in octal format (e.g., 755): ")
    # TODO: make this better? weird fix because pysftp wasn't converting string to octal properly
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        # username, password = user_pass()
        ssh.connect(host, port, username, password)
        sftp = ssh.open_sftp()       
        octal_permissions = int(permissions, 8)
        print(f"Converted octal permissions: {octal_permissions} (octal: {oct(octal_permissions)})")
        
        sftp.chmod(remote_path, octal_permissions)
        print(f"Permissions for {remote_path} successfully changed to {permissions}.")
        
        sftp.close()
        ssh.close()
    except ValueError as ve:
        print(f"Invalid permissions format: {ve}")
    except Exception as e:
        print(f"Failed to change permissions: {e}")

def print_help():
    print("""
Available commands:
  login                - Log into server
  logoff               - Log off the server
  list_remote          - List all files and directories on the server
  list_local           - List all local files and directories in a directory
  get                  - Download a file from the server (include file name in path)
  get_multiple         - Download multiple specified files from the server
  put                  - Upload a file to the server (include file name in path)
  put_multiple         - Upload multiple specified files to a remote directory
  delete               - Delete a file on the remote server (include file name in path)
  chmod                - Change permissions of a file on the remote server
  create               - Create a directory on the remote server
  exit                 - Exit the program
  help                 - Show this help message
    """)

def main():
    sftp = None
    print_help()
    inpt = ""
    while inpt != "exit":
        inpt = input("")
        match inpt:
            case "login":
                if sftp:
                    sftp.close()
                username, password = user_pass()
                sftp = connect(username, password)
                print_help()

            case "logoff":
                if sftp:
                    sftp.close()
                    sftp = None
                    print("Logged off successfully.")
                else:
                    print("No active connection to log off.")

            case "list_remote":
                if sftp:
                    list_all(sftp)
                else:
                    print("Not connected to any server.")

            case "list_local":
                list_all_local()

            case "get":
                if sftp:
                    download_file(sftp)
                else:
                    print("Not connected to any server.")

            case "get_multiple":
                if sftp:
                    download_files(sftp)
                else:
                    print("Not connected to any server.")

            case "put":
                if sftp:
                    upload_file(sftp)
                else:
                    print("Not connected to any server.")

            case "put_multiple":
                if sftp:
                    upload_files(sftp)
                else:
                    print("Not connected to any server.")

            case "delete":
                if sftp:
                    delete_file(sftp)
                else:
                    print("Not connected to any server.")

            case "create":
                if sftp:
                    create_directory(sftp)
                else:
                    print("Not connected to any server.")

            case "chmod":
                if sftp:
                    change_permissions(username, password)
                else:
                    print("Not connected to any server.")

            case "exit":
                if sftp:
                    sftp.close()
                print("Exiting the program.")

            case "help":
                print_help()

            case _:
                print("Unknown command. Type 'help' for a list of available commands.")

if __name__ == "__main__":
    main()
